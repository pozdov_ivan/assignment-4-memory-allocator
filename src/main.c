#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <sys/mman.h>

#include "mem.h"
#include "mem_internals.h"

#define PAGE_SIZE 4096

void test_correct_alloc()
{
    heap_init(REGION_MIN_SIZE);
    void *ptr = _malloc(100);
    assert(ptr);
    heap_term();
};

void test_free()
{
    heap_init(REGION_MIN_SIZE);
    void *ptr1 = _malloc(100);
    void *ptr2 = _malloc(100);
    void *ptr3 = _malloc(100);
    assert(ptr1 && ptr2 && ptr3);

    _free(ptr1);
    _free(ptr2);

    void *ptr4 = _malloc(100);
    assert(ptr4 && ptr4 < ptr3 && ptr1 == ptr4);

    heap_term();
}

void test_expand_region()
{
    heap_init(REGION_MIN_SIZE);
    void *ptr1 = _malloc(REGION_MIN_SIZE - 100);
    assert(ptr1);
    void *ptr2 = _malloc(1000);
    assert(ptr2);
    heap_term();
}

void test_separated_region()
{
    heap_init(REGION_MIN_SIZE);
    void *ptr = _malloc(REGION_MIN_SIZE - 100);

    intptr_t next_region = (intptr_t)ptr - ((intptr_t)ptr % REGION_MIN_SIZE) + REGION_MIN_SIZE;

    // take over next region and check if actually so
    void *next_region_allocated = mmap((void *)0 + next_region, 100, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, -1, 0);
    assert((intptr_t)next_region_allocated == next_region);

    void *ptr2 = _malloc(PAGE_SIZE);
    assert(ptr2);
    heap_term();
}

int main()
{
    test_correct_alloc();
    test_expand_region();
    test_free();
    test_separated_region();

    return 0;
}
